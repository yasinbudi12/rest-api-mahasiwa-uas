from django.contrib.auth.models import User, Group
from .models import Pasien,Penyakit
from rest_framework import serializers
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']
        

class PasienSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pasien
        fields = ['nama', 'umur']
class PenyakitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Penyakit
        fields = ['nama']
        